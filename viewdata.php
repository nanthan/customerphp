<?php

include 'dbconfig.php';


$array = [];
$sql ="SELECT * FROM customer";

if($result = mysqli_query($con,$sql))
{
  $i = 0;
  while($row = mysqli_fetch_assoc($result))
  {
    $array[$i]['id']    = $row['id'];
    $array[$i]['cname'] = $row['cname'];
    $array[$i]['shopname'] = $row['shopname'];
    $array[$i]['address'] = $row['address'];
    $i++;
  }

  echo json_encode($array);
}
else
{
  http_response_code(404);
}

 mysqli_close($con);
?>
